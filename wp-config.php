<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'carcouet' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'uCep/;6S!V%LsN=`l.2mR;]/ EX%UwR{.P4/sG~M$vM!UPb<y4Ge/NZ236a:bZ&)' );
define( 'SECURE_AUTH_KEY',  'X9b|#q([ QN?ER@RJUAhBU7u9ueG(~jO~q`*gP&?^#0jdUaNfzT._vusj9CVr+WJ' );
define( 'LOGGED_IN_KEY',    '0Sv!1usSM_|i-rj[1@Q_E3G^6R6ZF2_WW+|IR*dz_X]mRx&N2gf63,J}k`*jL_Nr' );
define( 'NONCE_KEY',        ')0e+=qS[3-%d<.w(*b9&;!T4V,/mJwezN;]FXX{zl[:0ZG?tKsV*JRsXc%d!5WV.' );
define( 'AUTH_SALT',        'H?w/f7GbmmG)WUfF<*#8^Yfy%=P4ZQJti|59>X[y2oQQ_Q5ref9X5_I4MW(i&8h_' );
define( 'SECURE_AUTH_SALT', '9vY&L]9yZWQk NTsG&>Yy/v,Mg/;B]XrA3h-fZx;.kF|)suH7OPKm*J.u:fTfVtg' );
define( 'LOGGED_IN_SALT',   'w,YT</GH,/Ee_v2z[E6li;N^`exmJT]I)%-h aTe[6GjCaV6Y7aja>3o6b<S! .h' );
define( 'NONCE_SALT',       'aXm,`o>a&]aPIPpz$B:Zi%0waKwsQ4#1dR~?a+a k?S@1uIsD>y9IZreTW qVpXZ' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
