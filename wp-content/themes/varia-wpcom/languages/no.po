msgid ""
msgstr ""
"Project-Id-Version: WordPress.com - Themes - Varia\n"
"Report-Msgid-Bugs-To: \n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2021-01-16T01:59:19+00:00\n"
"PO-Revision-Date: 2020-10-02 12:19:19+0000\n"
"Language: no\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: GlotPress/2.4.0-alpha\n"

#: inc/wpcom.php:59
msgid "Color Accessibility Warning"
msgstr "Fargetilgjengelighetsadvarsel"

#: inc/wpcom-colors.php:381
#: inc/wpcom-editor-colors.php:245
msgid "Secondary Color"
msgstr "Sekundærfarge"

#: inc/wpcom-colors.php:365
#: inc/wpcom-editor-colors.php:229
msgid "Text Color"
msgstr "Tekstfarge"

#: inc/wpcom-colors.php:216
#: inc/wpcom-editor-colors.php:153
msgid "Link Color"
msgstr "Lenkefarge"

#: inc/wpcom-colors.php:96
#: inc/wpcom-editor-colors.php:82
msgid "Background Color"
msgstr "Bakgrunnsfarge"

#: functions.php:174
msgid "Background"
msgstr "Bakgrunn"

#: functions.php:169
msgid "Foreground"
msgstr "Forgrunn"

#: inc/woocommerce.php:212
msgid "View your shopping list"
msgstr "Vis handlelisten din"

#: inc/woocommerce.php:208
msgid "Cart"
msgstr "Handlevogn"

#: inc/woocommerce.php:106
#: inc/woocommerce.php:142
msgid "%d item"
msgid_plural "%d items"
msgstr[0] "%d gjenstand"
msgstr[1] "%d gjenstander"

#: inc/woocommerce.php:103
#: inc/woocommerce.php:211
msgid "View your shopping cart"
msgstr "Vis handlevognen din"

#. translators: %s: Name of current post.
#: inc/template-functions.php:182
msgid "Continue reading %s"
msgstr "Fortsett å lese %s"

#: image.php:26
msgid "Next Image"
msgstr "Neste bilde"

#: image.php:25
msgid "Previous Image"
msgstr "Forrige bilde"

#: template-parts/post/author-bio.php:26
msgid "View more posts"
msgstr "Vis flere innlegg"

#. translators: %s: post author
#: template-parts/post/author-bio.php:17
msgid "Published by %s"
msgstr "Publisert av %s"

#. translators: %s: Name of current post. Only visible to screen readers
#: template-parts/content/content-single.php:33
msgid "Continue reading<span class=\"screen-reader-text\"> \"%s\"</span>"
msgstr "Fortsett å lese<span class=\"screen-reader-text\"> \"%s\"</span>"

#: template-parts/content/content-none.php:46
msgid "It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help."
msgstr "Vi klarer ikke å finne det du leter etter. Kanskje det kan hjelpe å søke."

#: template-parts/content/content-none.php:39
msgid "Sorry, but nothing matched your search terms. Please try again with some different keywords."
msgstr "Beklager, søket ditt ga ingen resultater. Prøv igjen med andre søkeord."

#. translators: 1: link to WP admin new post page.
#: template-parts/content/content-none.php:26
msgid "Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr "Klar for å publisere ditt første innlegg? <a href=\"%1$s\">Kom i gang her</a>."

#: template-parts/content/content-none.php:16
msgid "Nothing Found"
msgstr "Ingenting funnet"

#: template-parts/content/content-excerpt.php:18
#: template-parts/content/content.php:18
msgctxt "post"
msgid "Featured"
msgstr "Fremhevet"

#: single.php:42
msgid "Previous post:"
msgstr "Forrige innlegg:"

#: single.php:41
msgid "Previous Post"
msgstr "Forrige innlegg"

#: single.php:39
msgid "Next post:"
msgstr "Neste innlegg:"

#: single.php:38
msgid "Next Post"
msgstr "Neste innlegg"

#. translators: %s: parent post link
#: single.php:31
msgid "<span class=\"meta-nav\">Published in</span><br /><span class=\"post-title\">%s</span>"
msgstr "<span class=\"meta-nav\">Publisert i</span><span class=\"post-title\">%s</span>"

#. translators: 1: search result title. 2: search term.
#: search.php:25
msgid "Search results for:"
msgstr "Søkeresultater for:"

#: inc/wpcom.php:48
msgid "Hide Homepage Title"
msgstr "Skjul hjemmesidetittelen"

#: inc/template-tags.php:298
msgid "Older posts"
msgstr "Eldre innlegg"

#: inc/template-tags.php:294
msgid "Newer posts"
msgstr "Nyere innlegg"

#. translators: %s: Name of current post. Only visible to screen readers.
#. translators: %s: Name of current post. Only visible to screen readers
#: inc/template-tags.php:120
#: inc/template-tags.php:185
#: template-parts/content/content-page.php:40
msgid "Edit <span class=\"screen-reader-text\">%s</span>"
msgstr "Rediger <span class=\"screen-reader-text\">%s</span>"

#. translators: 1: SVG icon. 2: posted in label, only visible to screen
#. readers. 3: list of tags.
#: inc/template-tags.php:104
#: inc/template-tags.php:169
msgid "Tags:"
msgstr "Stikkord:"

#. translators: 1: SVG icon. 2: posted in label, only visible to screen
#. readers. 3: list of categories.
#: inc/template-tags.php:92
#: inc/template-tags.php:157
msgid "Posted in"
msgstr "Publisert i"

#. translators: used between list items, there is a space after the comma.
#: inc/template-tags.php:86
#: inc/template-tags.php:98
#: inc/template-tags.php:151
#: inc/template-tags.php:163
msgid ", "
msgstr ", "

#. translators: %s: Name of current post. Only visible to screen readers.
#: inc/template-tags.php:63
msgid "Leave a comment<span class=\"screen-reader-text\"> on %s</span>"
msgstr "Legg igjen en kommentar<span class=\"screen-reader-text\"> på %s</span>"

#. translators: 1: SVG icon. 2: post author, only visible to screen readers. 3:
#. author link.
#: inc/template-tags.php:46
msgid "Posted by"
msgstr "Skrevet av"

#: inc/template-functions.php:110
msgid "Archives:"
msgstr "Arkiver:"

#. translators: %s: Post type singular name
#. translators: %s: Taxonomy singular name
#: inc/template-functions.php:100
#: inc/template-functions.php:106
msgid "%s Archives"
msgstr "%s-arkiver:"

#: inc/template-functions.php:96
msgid "Daily Archives: "
msgstr "Daglige arkiver: "

#: inc/template-functions.php:94
msgctxt "monthly archives date format"
msgid "F Y"
msgstr "F Y"

#: inc/template-functions.php:94
msgid "Monthly Archives: "
msgstr "Månedlige arkiver: "

#: inc/template-functions.php:92
msgctxt "yearly archives date format"
msgid "Y"
msgstr "Å"

#: inc/template-functions.php:92
msgid "Yearly Archives: "
msgstr "Årlige arkiver:"

#: inc/template-functions.php:90
msgid "Author Archives: "
msgstr "Forfatterarkiver:"

#: inc/template-functions.php:88
msgid "Tag Archives: "
msgstr "Stikkordsarkiver:"

#: inc/template-functions.php:86
msgid "Category Archives: "
msgstr "Kategoriarkiver: "

#: image.php:93
msgctxt "Parent post link"
msgid "<span class=\"meta-nav\">Published in</span><br><span class=\"post-title\">%title</span>"
msgstr "<span class=\"meta-nav\">Publisert i</span><br><span class=\"post-title\">%title</span>"

#: image.php:76
msgctxt "Used before full size attachment link."
msgid "Full size"
msgstr "Full størrelse"

#: image.php:62
msgid "Page"
msgstr "Side"

#: image.php:58
#: template-parts/content/content-page.php:26
#: template-parts/content/content-single.php:46
#: template-parts/content/content.php:36
msgid "Pages:"
msgstr "Sider:"

#: header.php:56
#: inc/woocommerce.php:210
msgid "collapsed"
msgstr "Klappet sammen"

#: header.php:55
#: inc/woocommerce.php:209
msgid "expanded"
msgstr "Utvidet"

#: header.php:52
msgid "Menu"
msgstr "Meny"

#: header.php:49
msgid "Main Navigation"
msgstr "Hovednavigasjon"

#: header.php:31
msgid "Skip to content"
msgstr "Gå til innhold"

#: functions.php:207
msgid "Add widgets here to appear in your footer."
msgstr "Legg til moduler her som skal dukke opp i bunnområdet ditt."

#: functions.php:205
#: template-parts/footer/footer-widgets.php:12
msgid "Footer"
msgstr "Bunntekst"

#: functions.php:164
msgid "Secondary"
msgstr "Sekundær"

#: functions.php:136
msgid "XL"
msgstr "XL"

#: functions.php:135
msgid "Huge"
msgstr "Svær"

#: functions.php:130
msgid "L"
msgstr "L"

#: functions.php:129
msgid "Large"
msgstr "Stor"

#: functions.php:124
msgid "M"
msgstr "M"

#: functions.php:123
msgid "Normal"
msgstr "Normal"

#: functions.php:118
msgid "S"
msgstr "S"

#: functions.php:117
msgid "Small"
msgstr "Liten"

#: functions.php:61
#: header.php:71
msgid "Social Links Menu"
msgstr "Meny for sosiale lenker"

#: footer.php:29
#: functions.php:60
msgid "Footer Menu"
msgstr "Bunnmeny"

#: functions.php:59
#: functions.php:159
msgid "Primary"
msgstr "Hovedblogg"

#. translators: 1: WordPress link, 2: WordPress.
#: footer.php:50
msgid "https://wordpress.org/"
msgstr "https://wordpress.org/"

#: comments.php:66
msgid "Comments are closed."
msgstr "Stengt for kommentarer."

#. translators: 1: comment count number, 2: title.
#: comments.php:41
msgctxt "comments title"
msgid "%1$s thought on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s thoughts on &ldquo;%2$s&rdquo;"
msgstr[0] "%1$s kommentar om &ldquo;%2$s&rdquo;"
msgstr[1] "%1$s kommentarer om &ldquo;%2$s&rdquo;"

#. translators: 1: title.
#: comments.php:35
msgid "One thought on &ldquo;%1$s&rdquo;"
msgstr "En tanke om &ldquo;%1$s&rdquo;"

#: classes/class-varia-walker-comment.php:100
msgid "Your comment is awaiting moderation."
msgstr "Din kommentar avventer moderasjon."

#: classes/class-varia-walker-comment.php:95
msgid "Edit"
msgstr "Rediger"

#. translators: 1: comment date, 2: comment time
#: classes/class-varia-walker-comment.php:87
msgid "%1$s at %2$s"
msgstr "%1$s, kl. %2$s"

#. translators: %s: comment author link
#: classes/class-varia-walker-comment.php:67
msgid "%s <span class=\"screen-reader-text says\">says:</span>"
msgstr "%s <span class=\"screen-reader-text says\">sier:</span>"

#: 404.php:24
msgid "It looks like nothing was found at this location. Maybe try a search?"
msgstr "Det ble ikke funnet noe her. Kanskje du kan prøve å søke?"

#: 404.php:20
msgid "Oops! That page can&rsquo;t be found."
msgstr "Oops! Den siden finnes ikke."
