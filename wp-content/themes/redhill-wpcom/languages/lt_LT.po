# Translation of WordPress.com - Themes - Redhill in Lithuanian
# This file is distributed under the same license as the WordPress.com - Themes - Redhill package.
msgid ""
msgstr ""
"PO-Revision-Date: 2020-08-28 01:16:12+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: GlotPress/2.4.0-alpha\n"
"Language: lt\n"
"Project-Id-Version: WordPress.com - Themes - Redhill\n"

#: inc/wpcom-colors.php:465 inc/wpcom-editor-colors.php:230
msgid "Secondary Color"
msgstr "Antroji spalva"

#: inc/wpcom-colors.php:448 inc/wpcom-editor-colors.php:213
msgid "Text Color"
msgstr "Teksto spalva"

#: inc/wpcom-colors.php:285 inc/wpcom-editor-colors.php:117
msgid "Link Color"
msgstr "Nuorodų spalva"

#: inc/wpcom-colors.php:195 inc/wpcom-editor-colors.php:79
msgid "Background Color"
msgstr "Fono spalva"

#: functions.php:97
msgid "Foreground"
msgstr "Priekinis planas"

#: functions.php:92
msgid "Background"
msgstr "Fono nustatymai"

#. Description of the plugin/theme
#: wp-content/themes/pub/redhill/style.css
msgid "A simple theme with clean typography, created with entrepreneurs and small business owners in mind."
msgstr "Paprasta tema su švaria tipografija, sukurta verslininkams ir smulkiems verslams."

#: functions.php:87
msgid "Secondary"
msgstr "Antrinis"

#: functions.php:82
msgid "Primary"
msgstr "Pagrindinis"

#: functions.php:57
msgid "XL"
msgstr "XL"

#: functions.php:56
msgid "Huge"
msgstr "Labai didelis"

#: functions.php:51
msgid "L"
msgstr "L"

#: functions.php:50
msgid "Large"
msgstr "Didelis"

#: functions.php:45
msgid "M"
msgstr "M"

#: functions.php:44
msgid "Medium"
msgstr "Vidutinio tamsumo"

#: functions.php:39
msgid "N"
msgstr "N"

#: functions.php:38
msgid "Normal"
msgstr "Normalus"

#: functions.php:33
msgid "S"
msgstr "S"

#: functions.php:32
msgid "Small"
msgstr "Mažas"
